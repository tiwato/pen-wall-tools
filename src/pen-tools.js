Hooks.once("init", registerSettings)

Hooks.on("getSceneControlButtons", addControls)

function registerSettings() {

    WallsLayer.prototype._onClickLeft = (function (originalMethod) {
        return function (event) {
            var result = originalMethod.call(this, event);
            if (game.settings.get("pen-wall-tools", "CHAIN_WALLS")) this._chain = true
            return result
        }
    })(WallsLayer.prototype._onClickLeft);
    
    game.settings.register('pen-wall-tools', "CHAIN_WALLS", {
        scope: "world",
        type: Boolean,
        default: false,
        config: false
    });
}

function addControls(controls) {
    controls[4].tools.push(
        {
            name: "tile",
            title: "TOGGLECHAIN",
            icon: "fas fa-link",
            toggle: true,
            active: game.settings.get("pen-wall-tools", "CHAIN_WALLS"),
            onClick: toggled => game.settings.set("pen-wall-tools", "CHAIN_WALLS", toggled)
        },
        {
            name: "undo",
            title: "UNDOWALL",
            icon: "fas fa-undo",
            button: true,
            onClick: () => canvas.getLayer("WallsLayer").undoHistory()
        },
        {
            name: "Delete",
            title: "DELETEWALL",
            icon: "fas fa-eraser",
            button: true,
            onClick: () => canvas.getLayer("WallsLayer")._onDeleteKey()
        },
        {
            name: "big",
            title: "BIGBUTTON",
            icon: "fas fa-expand-alt",
            visible: true,
            button: true,
            onClick: () => {
                window.document.styleSheets[0].insertRule("#controls .control-tool, #controls .scene-control {width: 59px;height: 59px; font-size: 48px;line-height: revert;}", window.document.styleSheets[0].rules.length)
                window.document.styleSheets[0].insertRule("#controls .control-tools {left: 78px}", window.document.styleSheets[0].rules.length)
            }
        })

}

